// ==UserScript==
// @name         Governator Handy Hack
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  Always available remove site admin, Autofill site-admin
// @author       Richie Gee
// @match        https://governator.prod.atl-paas.net/app/tenant/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var sadiqaction = document.getElementById("tenant-actions");
    sadiqaction.innerHTML = '<ul><li><a data-action="temp-admin-verification-popup">Create temporary site admin</a></li><li><a data-action="remove-site-admin">Remove temporary site admin</a></li><li><a data-action="guest-admin-verification-popup">Grant other user temporary access</a></li><li><a data-action="show-update-tenant-tz-form">Change timezone for maintenance window</a></li></ul>';
    var urlParams = new URLSearchParams(window.location.search);
    var issueKey = urlParams.get('issueKey');
    document.getElementById("site-admin-issue").value=issueKey;
})();